import matplotlib.gridspec as gs
from matplotlib import pylab as plt
from pylab import *
from skimage import data, feature, morphology
from skimage.exposure import exposure
from skimage.filters import threshold_local, prewitt, sobel, roberts
from skimage.morphology import disk

warnings.simplefilter("ignore")


def main():
    images = [
        'lab4_images/samolot01.jpg',
        'lab4_images/samolot05.jpg',
        'lab4_images/samolot07.jpg',
        'lab4_images/samolot08.jpg',
        'lab4_images/samolot09.jpg',
        'lab4_images/samolot10.jpg',
    ]

    print(datetime.datetime.now().time())

    fig = plt.figure(figsize=(20, 20))

    grd = gs.GridSpec(3, 2)
    grd.update(wspace=0.05, hspace=0.05)

    for i, a in enumerate(images):
        print(i)
        ax1 = plt.subplot(grd[i])
        plt.axis('off')
        image = data.imread(a, as_grey=True)
        image = threshold_local(image, 15, offset=0.15)
        image = morphology.dilation(image, disk(1))
        ax1.imshow(exposure.adjust_gamma(prewitt(image), 2), cmap=cm.gray)

        print('ok')

    plt.savefig('lab4_output.pdf')
    plt.close()


if __name__ == '__main__':
    main()
