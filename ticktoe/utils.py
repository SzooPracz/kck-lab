# from __future__ import print_function
# from ipywidgets import interact, interactive, fixed, interact_manual
# import ipywidgets as widgets
import matplotlib.patches as mpatches

from skimage import data, io, color, img_as_ubyte, img_as_float64, img_as_uint, img_as_int
from skimage.feature import corner_harris, corner_subpix, corner_peaks,canny, match_template
from skimage.transform import warp, AffineTransform, resize, rescale, hough_ellipse, hough_circle, hough_circle_peaks, \
    hough_line, hough_line_peaks, rotate
from skimage.draw import ellipse, line_aa,ellipse_perimeter, circle_perimeter, line
from skimage.filters import threshold_otsu, threshold_local, prewitt, roberts, sobel, gaussian
from skimage.exposure import rescale_intensity, adjust_log, equalize_adapthist, histogram
from skimage.color import rgb2gray, label2rgb, gray2rgb
from skimage.measure import label, regionprops, find_contours
from skimage.morphology import closing, square, reconstruction, dilation, erosion
from skimage.segmentation import clear_border
from skimage.restoration import (denoise_tv_chambolle, denoise_bilateral, denoise_wavelet, estimate_sigma)
from skimage.util import img_as_ubyte
from scipy import ndimage as ndi
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
from matplotlib import cm



def regions(image):
    label_image = label(image, neighbors=8)
    all_regions = regionprops(label_image)
    all_regions = sorted(all_regions, key= lambda x : x.area, reverse=True)
    return all_regions

# def find_main_region(image):
#
#     box = image
#     box_regions = regions(box)
#     box_center = len(box)//2 , len(box[0])//2
#     closest_region = None
#     shortest_distance = 1000000.0
#     for region in box_regions:
#         distance = sqrt((region.centroid[0] - box_center[0])**2 + (region.centroid[1] - box_center[1])**2)
#         minr, minc, maxr, maxc = region.bbox
#         if distance < shortest_distance and minr <= box_center[0] and maxr >= box_center[0] and minc <= box_center[1] and maxc >= box_center[1]:
#             closest_region = region
#             shortest_distance = distance
#     return closest_region
def find_main_region(image):

    box = image
    box_regions = regions(box)
    box_center = len(box)//2 , len(box[0])//2
    greatest_region = None
    greatest_area = 0.0
    for region in box_regions:
        minr, minc, maxr, maxc = region.bbox
        if greatest_area < region.area and minr <= box_center[0] and maxr >= box_center[0] and minc <= box_center[1] and maxc >= box_center[1]:
            greatest_region = region
            greatest_area = region.area
    return greatest_region

def mark_circles(boolean_image):
    '''takes boolean gray image and returns rgb with red-drawn circles'''
    image = img_as_uint(boolean_image)
    hough_radii = np.arange(15, 20, 1)
    hough_res = hough_circle(image, hough_radii, normalize=True, full_output=False)
    # Select the most prominent circle
    accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                               threshold=0.45,
                                               num_peaks=2,
                                               total_num_peaks=2,
                                               normalize=True)
    image = gray2rgb(image)
    for center_y, center_x, radius in zip(cy, cx, radii):
        circy, circx = circle_perimeter(center_y, center_x, radius)
        try:
            image[circy, circx] = (255, 0,0)
        except IndexError:
            pass
    return image # returns uint image

def slice_into_3rows(picture):
    n = len(picture)
    d = n//30
    d = 0
    return (picture[0:n//3+d,] ,
            picture[n//3-d//2:2*n//3+d//2,],
            picture[2*n//3-d:n-1,])

def slice_into_3cols(row):
    n = len(row[0])
    d = n//30
    d = 0
    return (row[ :: , 0:n//3+d],
            row[ :: , n//3 -d//2 : 2*n//3 + d//2],
            row[ :: , 2*n//3 -d : n-1])

def slice_into_9(image):
    rows = slice_into_3rows(picture=image)
    all_boxes = []
    all_boxes.append(slice_into_3cols(rows[0]))
    all_boxes.append(slice_into_3cols(rows[1]))
    all_boxes.append(slice_into_3cols(rows[2]))
    return  all_boxes

def toe_pattern(size):
    '''returns uint rgb toe pattern in square of width equal given size
        size in range 20-40 is recommended'''
    toe = np.zeros((size,size,3))
    circx, circy = circle_perimeter(size//2,size//2, size//2-3)
    toe[circy, circx] = (255,255,255)
    toe = dilation(toe)
    # print(black)
    return toe

def tick_pattern(size):
    '''returns uint rgb tick pattern in square of width equal given size'''
    tick = np.zeros((size,size))
    rr, cc, val = line_aa(0,0,size-1,size-1)
    tick[rr, cc] = val
    rr, cc, val = line_aa(0,size-1,size-1,0)
    tick[rr, cc] = val
    tick = dilation(tick)
    tick[0,] = 0
    tick[size-1,] = 0
    tick[::,0] = 0
    tick[::, size-1] = 0
    return gray2rgb(tick)

def blank_pattern(size):
    '''returns rgb black image '''
    return np.zeros((size,size,3))


def show_9_in_grid(images):
    columns = 3
    rows = 3
    fig, ax_array = plt.subplots(rows,
                                 columns,
                                 squeeze=False,
                                 figsize=(10, 10),
                                 sharex=False,
                                 sharey=False)

    for i, ax_row in enumerate(ax_array):
        for j, axes in enumerate(ax_row):
            axes.set_title('')
            axes.axis('off')
            try:
                axes.imshow(images[i * 3 + j])
            except IndexError:
                break
    plt.show()

    fig.tight_layout()
    plt.show()






def present_hough_lines(image):
    # Classic straight-line Hough transform
    h, theta, d = hough_line(image)
    # Generating figure 1
    fig, axes = plt.subplots(1, 3, figsize=(15, 6))
    ax = axes.ravel()
    ax[0].imshow(image, cmap=cm.gray)
    ax[0].set_title('Input image')
    # ax[0].set_axis_off()

    ax[1].imshow(np.log(1 + h),
                 extent=[np.rad2deg(theta[-1]), np.rad2deg(theta[0]), d[-1], d[0]],
                 cmap=cm.gray, aspect=1 / 1.5)
    ax[1].set_title('Hough transform')
    ax[1].set_xlabel('Angles (degrees)')
    ax[1].set_ylabel('Distance (pixels)')
    ax[1].axis('image')

    ax[2].imshow(image, cmap=cm.gray)
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, num_peaks=4)):
        # print(angle, dist)
        y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
        y1 = (dist - image.shape[1] * np.cos(angle)) / np.sin(angle)
        ax[2].plot((0, image.shape[1]), (y0, y1), '-r')
    ax[2].set_xlim((0, image.shape[1]))
    ax[2].set_ylim((image.shape[0], 0))
    # ax[2].set_axis_off()
    ax[2].set_title('Detected lines')

    plt.tight_layout()
    plt.show()

def show_single(image, title = ''):
    fig, ax = plt.subplots(figsize=(10,10))
    ax.imshow(image)
    ax.set_title(title, fontsize=16)
    ax.set_axis_off()
    plt.show()

def show_histogram(gray_image):
    hist = histogram(gray_image)
    fig, ax = plt.subplots()
    ax.plot(hist[1],hist[0], lw=2)
    ax.set_title('Histogram of grey values')
    plt.tight_layout()
    plt.show()

def show_regions(image, title=''):
    fig, ax = plt.subplots(figsize=(10,6))
    ax.imshow(image)
    ax.set_title(title, fontsize=16)
    ax.set_axis_off()
    all_regions = regions(image)
    for region in all_regions:
        minr, minc, maxr, maxc = region.bbox
        rect = mpatches.Rectangle((minc, minr),maxc - minc, maxr - minr,
                                  fill=False, edgecolor='red', linewidth=2)
        ax.add_patch(rect)
    plt.show()

def adjust_angles(image):
    '''rotates an image with ticktoe baord to make it horizontal/vertical'''

    h, theta, d = hough_line(image)
    angles = []
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, min_distance=20, num_peaks=4)):
        angles.append(angle)
    angles = sorted(angles)
    if (len(angles) == 0):
        print('no hough lines found!')
    desired_angles = [-1.57, -1.57]
    deltas = [a-b for a, b in zip(angles[:2], desired_angles)]
    # result = rotate(image, np.degrees(min(deltas)))
    # result = rotate(image, np.degrees(np.mean(deltas)))
    result = rotate(image, np.degrees(max(deltas)))
    # show_single(found_box)
    # print(angles)
    return result

# fig, ax = plt.subplots(figsize=(10, 6))
# ax.imshow(tick_pattern(35))
# plt.tight_layout()
# plt.show()