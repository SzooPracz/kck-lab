import numpy as np
import scipy.signal as signal
import sys
import scipy.io.wavfile as wavfile
import warnings
import soundfile as sf


def func(file):
    samplerate, data = wavfile.read(file)
    data = data.astype(float) / 2 ** 16
    samplerate = float(samplerate)

    data_length = len(data)
    short = int(len(data) / 4)

    data = data[short:data_length - short]
    if not isinstance(data[0], float):
        data = data[:, 0] + data[:, 1]

    tr = signal.hamming(len(data))
    data_transformed = data * tr

    data_transformed = abs(np.fft.rfft(data_transformed))
    temp = len(data) / samplerate

    d3 = signal.decimate(data_transformed, 3)
    d5 = signal.decimate(data_transformed, 5)
    d7 = signal.decimate(data_transformed, 7)
    d9 = signal.decimate(data_transformed, 9)

    d2 = signal.decimate(data_transformed, 2)
    d4 = signal.decimate(data_transformed, 4)
    d6 = signal.decimate(data_transformed, 6)
    d8 = signal.decimate(data_transformed, 8)

    data_peak = data_transformed[:len(d9)] * d2[:len(d9)] * d3[:len(d9)] * d4[:len(d9)] * d5[:len(d9)] * d6[:len(d9)] * d7[:len(d9)] * d8[:len(d9)] * d9[:len(d9)]
    #data_peak = data_transformed[:len(d5)] * d2[:len(d5)] * d3[:len(d5)] * d5[:len(d5)] * d4[:len(d5)]

    final = 40 + np.argmax(data_peak[40:])

    result = final / temp
    print(str(samplerate), end=" ")
    # print(result)
    return result


if __name__ == '__main__':

    warnings.filterwarnings('ignore')
    data = sys.argv[1]
    try:
        result = func(data)
        if result > 150:
            print('K')

        else:
            print('M')

    except ValueError:
        print('K')
    except:
        print('K')

